<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('age')->nullable();
            $table->string('skin_type')->nullable();
            $table->string('skin_tan')->nullable();
            $table->string('pores_visibility')->nullable();
            $table->boolean('black_spots')->default(false);
            $table->string('wrinkles')->nullable();
            $table->string('skin_tightness')->nullable();
            $table->string('stains')->nullable();
            $table->string('akne')->nullable();
            $table->string('makeup')->nullable();
            $table->string('cleanup')->nullable();
            $table->boolean('purpose')->default(false);
            $table->boolean('subscribtion')->default(false);
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
