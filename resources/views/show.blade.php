@extends('layouts.admin')

@section('content')
    <div class="container results">
        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading">

                    <div class="row">
                        <div class="col-sm-9">
                            <h3 class="panel-title">{{$result->name or 'Anonymous Name'}} - {{$result->email  or 'Anonymous Email'}}</h3>
                        </div>
                        <div class="col-sm-3">
                            <a href="{{ url()->previous() }}" class="btn btn-default">Back</a>
                            @include('partials.logout')
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class=" col-md-5 col-lg-5 ">
                            <table class="table">
                                <tbody>

                                <tr>
                                    <td>Възраст</td>
                                    <td>{{$result->age}}</td>
                                </tr>
                                <tr>
                                    <td>Тип Кожа</td>
                                    <td>{{$result->skin_type}}</td>
                                </tr>
                                <tr>
                                    <td>Тенът ми е неравномерен</td>
                                    <td>{{$result->skin_tan}}</td>
                                </tr>
                                <tr>
                                    <td>Видимост</td>
                                    <td>{{$result->pores_visibility}}</td>
                                </tr>
                                <tr>
                                    <td>Имам черни точки</td>
                                    <td>{{$result->black_spots}}</td>
                                </tr>
                                <tr>
                                    <td>Наличие на бръчки</td>
                                    <td>{{$result->wrinkles}}</td>
                                </tr>
                                <tr>
                                    <td>Еластичност на кожата</td>
                                    <td>{{$result->skin_tightness}}</td>
                                </tr>
                                <tr>
                                    <td>Петна</td>
                                    <td>{{$result->stains}}</td>
                                </tr>
                                <tr>
                                    <td>Страдам от акне</td>
                                    <td>{{$result->akne}}</td>
                                </tr>
                                <tr>
                                    <td>Използвам грим</td>
                                    <td>{{$result->makeup}}</td>
                                </tr>
                                <tr>
                                    <td>Почиствам грима си</td>
                                    <td>{{$result->cleanup}}</td>
                                </tr>
                                <tr>
                                    <td>Purpose</td>
                                    <td>{{$result->purpose}}</td>
                                </tr>
                                <tr>
                                    <td>Subscribtion</td>
                                    <td>{{$result->subscribtion}}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-7 col-lg-7 recommended-products" align="center">
                            <h4>Recommended Products</h4>
                            <ul class="list-group list-group-flush">
                                @foreach ($result->products as $index=> $product)
                                    <li class="list-group-item">{{$product->title}}</li>
                                    @if($product->subitem_title)
                                        <li class="list-group-item">{{$product->subitem_title}}</li>
                                    @endif
                                @endforeach
                            </ul>

                            @foreach ($result->products as $index=> $product)
                                <img src="{{$product->imagePath()}}">
                                @if($product->subitem)
                                    <img src="{{$product->imagePathSubitem()}}">
                                @endif
                            @endforeach
                            <h4>{{$result->tip['title']}}</h4>

                            <ul class="list-group list-group-flush">
                                @foreach ($result->tip['tips'] as $tip )
                                    <li class="list-group-item">{{$tip}}</li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
