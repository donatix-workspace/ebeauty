<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="content-style-type" content="text/css" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="address=no" />

    <title>Нивеа Резултати</title>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
    table {border-collapse: collapse !important;}
</style>
<![endif]-->
    <style type="text/css">
        body {
            Margin: 0;
            padding: 0;
            min-width: 100%;
            background-color: #fff;
        }

        table {
            border-spacing: 0;
        }

        td {
            padding: 0;
        }

        img {
            border: 0;
        }

        p {
            margin: 0;
            padding: 0;
        }

        [office365] button {
            display: block !important;
        }

        div,
        button {
            margin: 0 !important;
            padding: 0 !important;
            display: block !important;
        }

        [owa] button {
            display: block!important;
        }

        * td {
            -webkit-text-size-adjust: none !important;
            font-size-adjust: none !important;
            -webkit-font-smoothing: antialiased !important;
            -mso-line-height: exactly;
        }

        table {
            border-collapse: collapse;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass * {
            line-height: 100%
        }

        * {
            -webkit-text-size-adjust: none
        }
    </style>
</head>

<body class="body" bgcolor="#ffffff" style="margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#fff;">
    <center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;">
        <table width="680" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#ffffff;"
            bgcolor="#ffffff" class="deviceWidth">
            <tr>
                <td align="center" style="padding-top:0px;padding-bottom:0;padding-right:0;padding-left:0;max-width:620px;width:100%;margin:0 auto;">
                    <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
                        bgcolor="#fff" class="deviceWidth">
                        <tr>
                            <td style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;' height="60">

                            </td>
                        </tr>
                    </table>
                    <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
                        bgcolor="#fff" class="deviceWidth">
                        <tr>
                            <td align="center" valign="top" style='font-family:"Arial Black", Gadget, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                                height="30">
                                Здравейте, {{$result->name}}
                            </td>
                        </tr>
                    </table>
                    <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
                        bgcolor="#fff" class="deviceWidth">
                        <tr>
                            <td style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;' height="30">

                            </td>
                        </tr>
                    </table>
                    <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
                        bgcolor="#fff" class="deviceWidth">
                        <tr>
                            <td align="center" valign="top" style='font-family:"Arial Black", Gadget, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                                height="30">
                                Подходящите за Вашата кожа продукти, според отговорите Ви в анкетата са:
                            </td>
                        </tr>
                    </table>
                    <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
                    bgcolor="#fff" class="deviceWidth">
                    <tr>
                        <td style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;' height="80">

                        </td>
                    </tr>
                </table>
                    <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
                        bgcolor="#fff" class="deviceWidth">
                        <tr>
                            @foreach ($result->products as$index => $product)


                            <td align="center" width="200" valign="bottom" style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                                >
                                <img src="{{ $product->imagePath() }}" style="display:block;max-width:100%;" />
                                @if($product->subitem)
                                    <br/>
                                    <img src="{{ $product->imagePathSubitem() }}" style="display:block;max-width:100%;" />
                                    @endif
                            </td>
                            <td align="center" width="40" valign="bottom" style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                                >

                            </td>
                            @endforeach

                        </tr>
                    </table>
                    <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
                    bgcolor="#fff" class="deviceWidth">
                    <tr>
                        <td style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;' height="80">

                        </td>
                    </tr>
                </table>
                <table width="700" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
                    bgcolor="#fff" class="deviceWidth">
                    @foreach ($result->products as $indexKey => $product)
                    <tr>
                        <td align="left" width="100" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'       height="30">
                            <img src="{{ asset('img/tips'.($indexKey+1).'.png') }}"/>
                        </td>
                        <td align="left" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                            height="30">
                            {{$product->title}}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                            height="30">
                        </td>
                    </tr>
                @endforeach



                </table>
                <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
                bgcolor="#fff" class="deviceWidth">
                <tr>
                    <td style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;' height="80">

                    </td>
                </tr>
            </table>
            <table width="700" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
            bgcolor="#fff" class="deviceWidth">
            <tr>
                <td align="left" valign="top" style='font-family:"Arial Black", Gadget, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                    height="30">
                    {{$result->tip['title']}}:
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                    height="40">
                </td>
            </tr>

            @foreach ($result->tip['tips'] as $tip)
                <tr>
                    <td align="left" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                        height="30">
                        -{{$tip}}
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                        height="40">
                    </td>
                </tr>
            @endforeach
        </table>
        <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
        bgcolor="#fff" class="deviceWidth">
        <tr>
            <td style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;' height="100">

            </td>
        </tr>
    </table>
        <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;" bgcolor="#fff" class="deviceWidth">
                <tr>
                    <td align="center" width="266" valign="bottom" style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;'
                        >
                        <a href="https://www.nivea.bg/" target="_blank"><img src="{{ asset('img/logo_nivea.jpg') }}" style="display:block;max-width:100%;" /></a>
                    </td>
                </tr>
            </table>
            <table width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="border-spacing:0;background-color:#fff;"
            bgcolor="#fff" class="deviceWidth">
            <tr>
                <td style='font-family:"Trebuchet MS", Helvetica, sans-serif; font-weight:bold; font-size:30px; color:#00136f;' height="80">

                </td>
            </tr>
        </table>
                </td>
            </tr>
        </table>
        <!-- close the main table 100%-->
        <!-- DO NOT DELETE table added to force the display of desktop version on apps that don't support media queries-->
        <table border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%;border-spacing:0;" class="deviceWidth">
            <tr>
                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="680" style="width:700px;border-spacing:0;" class="deviceWidth">
                        <tr>
                            <td height="1" style="line-height:1px;width:700px;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                               <img src="{{ asset('img/dot.gif') }}" width="700" height="1" alt=" " style="display:block;height:1px;width:700px;border-width:0;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- //DO NOT DELETE table added to force the display of desktop version on apps that don't support media queries-->
    </center>
</body>

</html>
