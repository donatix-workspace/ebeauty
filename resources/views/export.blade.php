<!DOCTYPE html>
<html>
<head>
    <title>Поръчки</title>
    <font face="arial">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style type="text/css">
            td {
                padding-right: 10px;
            }
        </style>
        </head>
<body>
<table class="display" width="100%" cellspacing="0">
    <tr>
        <td>Обект</td>
        <td>Възраст</td>
        <td>Тип Кожа</td>
        <td>Тенът ми е неравномерен</td>
        <td>Видимост</td>
        <td>Имам черни точки</td>
        <td>Наличие на бръчки</td>
        <td>Еластичност на кожата</td>
        <td>Петна</td>
        <td>Страдам от акне</td>
        <td>Използвам грим</td>
        <td>Почиствам грима си</td>
        <td>Цели на приложението</td>
        <td>Абонамент</td>
        <td>Име</td>
        <td>email</td>
        <td>Година</td>
        <td>Дата</td>
        <td>Време</td>
        <td>Продукти</td>
    </tr>
    @foreach ($results as $result)
        <tr>
            <td>{{$result->venue_name or 'No venue'}}</td>
            <td>{{$result->age}}</td>
            <td>{{$result->skin_type}}</td>
            <td>{{$result->skin_tan}}</td>
            <td>{{$result->pores_visibility}}</td>
            <td>{{$result->black_spots}}</td>
            <td>{{$result->wrinkles}}</td>
            <td>{{$result->skin_tightness}}</td>
            <td>{{$result->stains}}</td>
            <td>{{$result->akne}}</td>
            <td>{{$result->makeup}}</td>
            <td>{{$result->cleanup}}</td>
            <td>{{$result->purpose}}</td>
            <td>{{$result->subscribtion}}</td>
            <td>{{$result->name or 'Anonymous Name'}}</td>
            <td>{{$result->email or 'Anonymous Email'}}</td>
            <td>{{(new \Carbon\Carbon($result->created_at))->format('Y')}}</td>
            <td>{{(new \Carbon\Carbon($result->created_at))->format('F d')}}</td>
            <td>{{(new \Carbon\Carbon($result->created_at))->format('H:m')}}</td>
            <td>
                @foreach ($result->products as $product)
                    {{$product->title}}
                    <br>
                    @if($product->subitem_title)
                        {{$product->subitem_title}}
                        <br>
                    @endif
                @endforeach
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>