@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-7">
                        <h3 class="panel-title">Results</h3>
                    </div>
                    <div class="col-sm-4">
                        <form action="{{route('results.export')}}" method="post">
                        <h1 class="panel-title">Limit results to:</h1>
                        <input type="date" name="date_to">
                        <input type="submit" value="Export">
                            {{ csrf_field() }}
                    </form>
                    </div>
                    <div class="col-sm-1">
                        {{--<a class="btn btn-default" href="{{route('results.export')}}"> Export</a>--}}
                        @include('partials.logout')
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Venue</th>
                        <th scope="col">Name</th>
                        <th scope="col">Е-mail</th>
                        <th scope="col">Questions</th>
                        <th scope="col">Recommended Products</th>
                        <th scope="col">Year</th>
                        <th scope="col">Date</th>
                        <th scope="col">Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($results as $index => $result)
                        <tr>
                            <td>{{$result->id}}</td>
                            <td>{{$result->venue_name or 'No venue'}}</td>
                            <td>{{$result->name or "Anonymous Name"}}</td>
                            <td>{{$result->email or "Anonymous Email" }}</td>
                            <td><a href="{{route('questions.show',$result)}}">Details</a></td>
                            <td>
                                <ul>
                                    @foreach ($result->products as $product)
                                        <li>{{$product->title}}</li>
                                        @if($product->subitem_title)
                                            <li>{{$product->subitem_title}}</li>
                                        @endif
                                    @endforeach
                                </ul>
                            </td>
                            <td>{{(new \Carbon\Carbon($result->created_at))->format('Y')}}</td>
                            <td>{{(new \Carbon\Carbon($result->created_at))->format('F d')}}</td>
                            <td>{{(new \Carbon\Carbon($result->created_at))->format('H:m')}}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                <div align="center">{{ $results->links()}}</div>
            </div>
        </div>
    </div>
    </div>
@endsection
