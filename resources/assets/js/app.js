
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueFormWizard from 'vue-form-wizard';
import 'vue-form-wizard/dist/vue-form-wizard.min.css';

Vue.use(VueFormWizard)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('ebeauty', require('./components/ebeauty.vue'));
Vue.component('firstQuestions', require('./components/firstQuestions.vue'));
Vue.component('secondQuestions', require('./components/secondQuestions.vue'));
Vue.component('products', require('./components/products.vue'));
Vue.component('userDetails', require('./components/userDetails.vue'));
Vue.component('skinSlired', require('./components/skinSlider.vue'));
Vue.component('modal', require('./components/modal.vue'));
//global registration


const app = new Vue({
    el: '#app'
});
