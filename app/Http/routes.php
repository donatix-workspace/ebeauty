<?php

Route::get('/', function () {
    return view('content_holder', [
        'content' => file_get_contents(public_path('static/index.html'))
    ]);
});

Route::auth();

Route::get('/questions', function () {
    return view('questions', [
        'imagesMapping' => file_get_contents(public_path('mix-manifest.json'))
    ]);
});
Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin', ['as' => 'results.index', 'uses' => 'ResultController@index']);
    Route::get('/admin/questions/{result}', ['as' => 'questions.show', 'uses' => 'ResultController@show']);
    Route::post('/admin/results/export', ['as' => 'results.export', 'uses' => 'ResultController@export']);
});


