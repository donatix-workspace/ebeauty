<?php

namespace App\Http\Controllers;

use App\Mail\ResultReceive;
use App\Product;
use App\Result;
use App\Services\ProCampaignService;
use Carbon\Carbon;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class ResultController extends Controller
{


    public function index()
    {
        return view('index', [
            'results' => Result::with('products')->orderBy('created_at', 'desc')->paginate(15)]);
    }

    public function show(Result $result)
    {
        return view('show', [
            'result' => $result->load(['products'])
        ]);
    }

    public function store(Request $request)
    {

        $resultArray = [
            'age' => $request->get('age'),
            'skin_type' => $request->get('skin_type'),
            'skin_tan' => $request->get('skin_tan'),
            'pores_visibility' => $request->get('pores_visibility'),
            'black_spots' => $request->get('black_spots'),
            'wrinkles' => $request->get('wrinkles'),
            'skin_tightness' => $request->get('skin_tightness'),
            'stains' => $request->get('stains'),
            'akne' => $request->get('akne'),
            'makeup' => $request->get('makeup'),
            'cleanup' => json_encode($request->get('cleanup')),
            'purpose' => $request->get('purpose'),
            'subscribtion' => $request->get('subscribtion'),
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'venue_name' => $request->get('venue_name'),
        ];

        if(!isset($resultArray['subscribtion']) || ((bool) $resultArray['subscribtion']) == false) {
            $resultArray['name'] = null;
            $resultArray['email'] = null;
        }

        $result = Result::create($resultArray);
        $results = [];
        foreach ($request->get('recomended') as $productRecord) {
            $results[] = new Product([
                'product_id' => $productRecord['id'],
                'title' => $productRecord['title'],
                'weight' => $productRecord['weight'],
                'subitem' => isset($productRecord['subitem']['id']) ? $productRecord['subitem']['id'] : null,
                'subitem_title' => isset($productRecord['subitem']['title']) ? $productRecord['subitem']['title'] : null,
            ]);
        }
        $result->products()->saveMany($results);

        if ($request->has(['email', 'name']) && !App::environment('local')) {
            try {
                $service = new ProCampaignService($request->get('email'), $request->get('name'), $result);
                $service->triggerMail();
            }
            catch (ClientException $e) {
                logger()->debug('ProCampaign exception');
                logger()->debug('Request headers: ', ['request' => $e->getRequest()->getHeaders()]);
                logger()->debug('Request body: ', ['request' => $e->getRequest()->getBody()]);
                logger()->debug('Request url: ', ['request' => $e->getRequest()->getUri()]);
                logger()->debug('Request method: ', ['request' => $e->getRequest()->getMethod()]);
                logger()->debug('Response: ', ['response' => $e->getResponse()->getBody()]);

                throw new \Exception("{$e->getResponse()->getBody()}");
            }
        }


        return response()->json(['message' => 'success'], 200);
    }

    public function export(Request $request)
    {
        $query = Result::query();

        if ($request->has('date_to')) {
            $query->where('created_at','>=', new Carbon($request->all()['date_to']));
        }

        $exportName = 'export_' . Carbon::now()->format('Ymd');
        Excel::create($exportName, function ($excel) use ($query) {
            $excel->sheet('Export', function ($sheet) use ($query) {
                $sheet->setfitToWidth(true);
                $sheet->loadView('export', ['results' => $query->with('products')->orderBy('created_at', 'desc')->get()]);
            });
        })->download('csv');

    }

}
