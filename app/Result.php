<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = ['age', 'skin_type', 'skin_tan', 'pores_visibility', 'black_spots', 'wrinkles', 'skin_tightness', 'stains', 'akne', 'makeup', 'cleanup', 'purpose', 'subscribtion', 'name', 'email', 'venue_name',];

    protected $ages = [
        '0'=>'18-25',
        '1'=>'26-30',
        '2'=>'31-35',
        '3'=>'36-45',
        '4'=>'46-55',
        '5'=>'55+',
    ];
    protected $skinTypes = [
        '0'=>'ЧУВСТВИТЕЛНА',
        '1'=>'СУХА',
        '2'=>'НОРМАЛНА',
        '3'=>'СМЕСЕНА',
        '4'=>'МАЗНА',
    ];
    protected $skinTans = [
        '0'=>'НЕ',
        '1'=>'МАЛКО',
        '2'=>'МНОГО',
    ];
    protected $poresVisibilities = [
        '0'=>'НЕЗАБЕЛЕЖИМИ',
        '1'=>'ВИДИМИ',
    ];
    protected $blackSpots = [
        0=>'НЕ',
        1=>'ДА',
    ];
    protected $wrinkleses = [
        '0'=>'НЯМА',
        '1'=>'ФИНИ, МИМИЧЕСКИ',
        '2'=>'ВИДИМИ',
        '3'=>'ДЪЛБОКИ',
    ];
    protected $skinTightnesses = [
        '0'=>'ПЛЪТНА И ЕЛАСТИЧНА',
        '1'=>'НАМАЛЕНА',
        '2'=>'ВИДИМО ОТПУСНАТА',
        '3'=>'ДЪЛБОКИ',
    ];
    protected $stainses = [
        '0'=>'НЕ',
        '1'=>'НА МЕСТА',
        '2'=>'ДА',
    ];
    protected $aknes = [
        '0'=>'НЕ',
        '1'=>'ДА',
    ];
    protected $makeups = [
        '0'=>'НЕ ИЗПОЛЗВАМ',
        '1'=>'Само околоочен',
        '2'=>'Лек грим',
        '3'=>'Дълготраен',
    ];
    protected $cleanups = [
        '0'=>'С памучни тампони',
        '1'=>'С Измиващи продукти',
        '2'=>'С ПОЧИСТВАЩИ КЪРПИЧКИ',
        '3'=>'НЕ СИ ПОЧИСТВАМ ГРИМА ЕЖЕДНЕВНО',
    ];
    protected $bools = [
        0=>'НЕ',
        1=>'ДА',
    ];

    protected $tips = [
        '0'=>[
            'title'=>'Съвети за чувствителна кожа',
            'tips'=>[
                'Нежно потупвайте лицето си с кърпа, след като го почистите, вместо да го търкате.',
                'Обръщайте внимание на храненето си – пикантните подправки не са полезни за кожата ви.',
                'Изберете крем, който осигурява защита от вредните влияния на околната среда.'
            ]
        ],
        '1'=>[
            'title'=>'Съвети за суха кожа',
            'tips'=>[
                'Можете да помогнете на кожата си, като се храните здравословно. Пийте по 2 литра вода всеки ден.',
                'Дневните кремове със слънцезащитен фактор защитават кожата ви от UV лъчението.',
                'Продуктите за грижа с естествени масла облекчават кожата.'
            ]
        ],
        '2'=>[
            'title'=>'Съвети за нормална кожа',
            'tips'=>[
                'Редовната дневна и нощна грижа поддържа кожата ви свежа.',
                'Нежното ексфолиране премахва нечистотите и мъртвите кожни клетки.',
                'Като нанасяте крема си за лице с нежни масажни движения, стимулирате кръвообращението.'
            ]
        ],
        '3'=>[
            'title'=>'Съвети за смесена кожа',
            'tips'=>[
                'Почиствайте внимателно кожата си преди нанасянето на продукти за грижа за кожата на лицето.',
                'Използвайте хладка вода вместо гореща или леденостудена.',
                'Редовно използвайте хидратиращ продукт – количество с размер на лешник е достатъчно!'
            ]
        ],
        '4'=>[
            'title'=>'Съвети за мазна кожа',
            'tips'=>[
                'Изключително важно е да премахвате грима си вечер, защото през нощта той може да запуши порите ви.',
                'Третирайте кожата си с нежен скраб и маска за лице веднъж седмично.',
                'Долу ръцете: не стискайте черните точки или пъпките!'
            ]
        ],
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function getAgeAttribute($value)
    {
        return $this->ages[(int) $value];
    }

    public function getSkinTypeAttribute($value)
    {
        return $this->skinTypes[(int) $value];
    }
    public function getSkinTanAttribute($value)
    {
        return $this->skinTans[(int)$value];
    }
    public function getPoresVisibilityAttribute($value)
    {
        return $this->poresVisibilities[(int)$value];
    }
    public function getBlackSpotsAttribute($value)
    {
        return $this->blackSpots[(int)$value];
    }
    public function getWrinklesAttribute($value)
    {
        return $this->wrinkleses[(int)$value];
    }

    public function getSkinTightnessAttribute($value)
    {
        return $this->skinTightnesses[(int)$value];
    }
    public function getStainsAttribute($value)
    {
        return $this->stainses[(int)$value];
    }
    public function getAkneAttribute($value)
    {
        return $this->aknes[(int)$value];
    }
    public function getMakeupAttribute($value)
    {
        return $this->makeups[(int)$value];
    }
    public function getCleanupAttribute($value)
    {
        $result = '';
        foreach (json_decode($value) as $cleanup){
            $result.=$this->cleanups[$cleanup].' ';
        }
        return rtrim($result," ");
    }
    public function getPurposeAttribute($value)
    {
        return $this->bools[(int)$value];
    }
    public function getSubscribtionAttribute($value)
    {
        return $this->bools[(int)$value];
    }
    public function getTipAttribute()
    {
        return $this->tips[$this->attributes['skin_type']];
    }

    public function getCreatedAtAttribute()
    {
        return date('F d, Y H:m', strtotime($this->attributes['created_at']));
    }
}
