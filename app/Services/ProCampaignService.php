<?php

namespace App\Services;


use GuzzleHttp\Client;

class ProCampaignService
{
    protected $client;
    protected $url;
    protected $request;
    protected $email;
    protected $subscribtion;
    protected $name;
    protected $data;

    public function __construct($mail, $name, $data)
    {
        $this->client = new Client(['headers' => ['Content-Type' => 'application/json']]);
        $this->url = env('PROCAMPAIGN_URL');
        $this->request = [
            'query' => [
                'apiKey' => env('PROCAMPAIGN_API_KEY')
            ],
        ];
        $this->email = $mail;
        $this->name = $name;
        $this->subscribtion = $data->getOriginal('subscribtion');
        $this->data = $data;
    }

    public function triggerMail()
    {
        $parameters = [
            [
                'name' => 'EB_Name', 'value' => $this->name
            ],
            [
                'name' => 'Email', 'value' => $this->email
            ],
            [
                'name' => 'Product1Name',
                'value' => $this->data['products'][0]['title']
            ],
            [
                'name' => 'Product1Image',
                'value' => $this->data['products'][0]->imagePath()
            ],
            [
                'name' => 'Product2Name',
                'value' => $this->data['products'][1]['title']
            ],
            [
                'name' => 'Product2Image1',
                'value' => $this->data['products'][1]->imagePath()
            ],
            [
                'name' => 'Product3Name1',
                'value' => $this->data['products'][2]['title']
            ],
            [
                'name' => 'Product3Image1',
                'value' => $this->data['products'][2]->imagePath()
            ],
            [
                'name' => 'EB_Tip1Title',
                'value' => $this->data['tip']['title']
            ],
            [
                'name' => 'EB_Tip1Content',
                'value' => $this->data['tip']['tips'][0]
            ],
            [
                'name' => 'EB_Tip2Content',
                'value' => $this->data['tip']['tips'][1]
            ],
            [
                'name' => 'EB_Tip3Content',
                'value' => $this->data['tip']['tips'][2]
            ],

        ];

        if (isset($this->data['products'][1]['subitem'])) {
            $parameters[] =
                [
                    'name' => 'Product2Image2',
                    'value' => $this->data['products'][1]->imagePathSubitem()
                ];
            $parameters[] =
                [
                    'name' => 'Product2Name2',
                    'value' => $this->data['products'][1]['subitem_title']
                ];
        }

        if (isset($this->data['products'][2]['subitem'])) {
            $parameters[] =
                [
                    'name' => 'Product3Image2',
                    'value' => $this->data['products'][2]->imagePathSubitem()
                ];
            $parameters[] =
                [
                    'name' => 'Product3Name2',
                    'value' => $this->data['products'][2]['subitem_title']
                ];
        }

        $this->request['body'] = json_encode([
            'Source' => 'eb1',
            'Attributes' => $parameters,
            'Transactions' => [
                [
                    'name' => 'Ebeauty_Results',
                ],
                [
                    "name" => "NVZZstandards Nivea Newsletter Subscription (IN)",
                    "parameters" => [
                        [
                            'name' => 'Q1',
                            'value' => $this->subscribtion ? 1:0
                        ]
                    ],
                ]
            ]]);
        //https://consumer.procampaignapi.com/consumer?apiKey=TklWRUEuQkcgVjd8ZWIx
        return $this->client->request('POST', $this->url, $this->request);
    }
}
