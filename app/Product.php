<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['product_id', 'title', 'weight', 'subitem', 'subitem_title',];

    public function result()
    {
        return $this->belongsTo('App\Result');
    }

    public function imagePath()
    {
        return config('app.url') . config('app.img_path') . $this->product_id . '.png';
    }

    public function imagePathSubitem()
    {
        return config('app.url') . config('app.img_path') . $this->subitem . '.png';
    }
}
