# Nivea e-beauty
## Getting Started
1. Ensure you have
* PHP >= 5.6.4
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Mysql/MariaDB
* Composer installed


1. Clone the repo to your localhost directory - 
`git clone http://git.donatix.info/beiersdorf/ebeauty.git`
1. In project directory copy `.env.example` and rename it to `.env`
1. Create database and replace db creadentials in `.env` - `DB_DATABASE=homestead`
                                                           `DB_USERNAME=homestead`
                                                           `DB_PASSWORD=secret`
1. Open git bash in your project directory and run
* `composer update`
* `php artisan key:generate`
* `php artisan migrate`

1. Open project from your localhost
*Ex:* `http://127.0.0.1/ebeauty/public/`

## Deploy

After deploy run `php artisan key:generate` to create new versions for css and js in order to overwrite the cache